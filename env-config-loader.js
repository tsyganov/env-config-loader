var _ = require('underscore');
var path = require('path');

var _environment;

var getNormalizedPath = function(configFileName) {
    return  path.normalize(path.dirname(module.parent.filename) + '/' + configFileName);
};

module.exports.setEnvironment = function(environment) {
    _environment = environment;
};

module.exports.load = function (configFileName) {

    var configFileParts = configFileName.split(".");
    configFileParts.splice(configFileParts.length - 1, 0, _environment);

    var subConfigFileName = configFileParts.join(".");
    var subConfig = {};

    try {
        subConfig = require(getNormalizedPath(subConfigFileName));
    } catch (e) {}

    return _.extend(
        require(getNormalizedPath(configFileName)),
        subConfig
    );
};