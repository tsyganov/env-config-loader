var assert = require('assert');

var moduleName = '../env-config-loader';
var loader = require(moduleName);

describe('Environment config loader', function() {
    it('still remembers set environment even if set from another file', function() {
        var values = loader.load('./static/test2.json');
        assert.deepEqual(values, {
            "key1": "value1",
            "key2": "value4",
            "key3": "value5"
        });
    })
});